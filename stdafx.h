#pragma once

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

// Windows Header Files:
#include <windows.h>

// Local Header Files

// TODO: reference additional headers your program requires here
#define AFX_EXT_CLASS __declspec(dllexport)

#if _MSC_VER < 1900 
#define snprintf _snprintf
#endif