#include "stdafx.h"
#include <Windows.h>
#include <atlbase.h>

/* Exported functions */
extern "C" {
	AFX_EXT_CLASS INT bpNotifyCallEvents(BOOL isIncoming, LPWSTR contactName, LPWSTR contactNumber, LPWSTR originatingNumber, INT callId, INT callStatus);
	AFX_EXT_CLASS INT bpNotifyCallExtraData(INT callId, LPWSTR extraInfo);
} // extern "C"
