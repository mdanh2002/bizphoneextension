#include "stdafx.h"
#include "atlbase.h"
#include "BizPhoneExtension.h"

/*
	This method is called when there is an outgoing call or incoming call event is received.
	Method should return negative code to indicate failure, zero or positive to indicate success.

	Parameters:

	isIncoming - TRUE if the call is an incoming call, FALSE if the call is an outgoing call
	contactName - Name of the caller or contact
	contactNumber - Number of caller or contact
	originatingNumber - For incoming calls, this parameter contains the number which was called by the caller, if available.
	callId - The ID of the call
	callStatus - The status of the call. 0=NULL, 1=CALLING, 2=INCOMING, 3=EARLY, 4=CONNECTING, 5=CONFIRMED, 6=DISCONNECTED
*/
INT bpNotifyCallEvents(BOOL isIncoming, LPWSTR contactName, LPWSTR contactNumber, LPWSTR originatingNumber, INT callId, INT callStatus)
{
	/*
	char callInfo[500];
	sprintf(callInfo, "isIncoming=%d, contactName=%S, contactNumber=%S, originatingNumber=%S, callId=%d, callStatus=%d", 
						isIncoming, contactName, contactNumber, originatingNumber, callId, callStatus);

	MessageBoxA(GetActiveWindow(), callInfo, "Call Info", MB_OK);
	*/

	return 0;
}

INT bpNotifyCallExtraData(INT callId, LPWSTR extraInfo)
{
	// MessageBoxW(GetActiveWindow(), extraInfo, L"Call Info", MB_OK);
	return 0;
}